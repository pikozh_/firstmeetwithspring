package com.example.demo.configuration;

import com.example.demo.repository.UserRepositoryService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConfigurationApp {

    @Bean(name="repositoryService")
    public UserRepositoryService repositoryService(){
        return new UserRepositoryService();
    }

}
