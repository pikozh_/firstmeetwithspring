package com.example.demo.controllers;

import com.example.demo.controllers.dto.LoginRequest;
import com.example.demo.repository.UserRepositoryService;
import com.example.demo.repository.entity.UserEntity;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

@Slf4j
@RestController
@RequestMapping("/api")
public class RestApi {

    private final UserRepositoryService service;

    public RestApi(UserRepositoryService userRepositoryService) {
        this.service = userRepositoryService;
    }

    @PostMapping("/login")
    public void login(@RequestBody LoginRequest login) {

        try {
            if (login == null) {
                throw new IllegalArgumentException("login is null");
            }
            if (service.verifyUser(login)) {

            } else {
                throw new IllegalArgumentException("This user is unrecognized");
            }
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED,
                    e.getMessage());
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @PutMapping("/sign-up")
    public void userSave(@RequestBody LoginRequest request) {
        try {
            if (request == null) {
                throw new IllegalArgumentException("request is empty");
            }
            service.saveUser(new UserEntity(request.getName(), request.getPassword()));

        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NO_CONTENT,
                    e.getMessage());
        } catch (Exception e) {
            log.error("Sign-up error ", e);
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }


}
