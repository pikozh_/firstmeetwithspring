package com.example.demo.repository;

import com.example.demo.controllers.dto.LoginRequest;
import com.example.demo.repository.entity.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserRepositoryService {

    private UserRepository userRepository;

    public boolean verifyUser(LoginRequest login) {
        UserEntity user = userRepository.findUserByNameAndPassword(login.getName(), login.getPassword());

        return user != null;
    }

    public void saveUser(UserEntity userEntity) {
        userRepository.save(userEntity);
    }

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

}
